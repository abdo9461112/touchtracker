//
//  Line.swift
//  TouchTracker
//
//  Created by abdo emad  on 09/02/2024.
//

import Foundation
import CoreGraphics

struct Line {
    var begin = CGPoint.zero
    var end = CGPoint.zero

}
